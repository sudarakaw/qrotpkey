/**
 * src/index.js: main entry point
 *
 * Copyright 2018 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
 *
 * This program comes with ABSOLUTELY NO WARRANTY;
 * This is free software, and you are welcome to redistribute it and/or modify
 * it under the terms of the BSD 2-clause License. See the LICENSE file for more
 * details.
 *
 */

const
  yargs = require('yargs'),
  { PNG } = require('pngjs'),
  qr = require('jsqr'),
  { authenticator } = require('otplib'),
  Table = require('cli-table'),
  colors = require('colors'),
  fs = require('fs'),
  fsp = fs.promises,
  path = require('path'),

  { argv } = yargs
    .usage(`USAGE: ./$0 <path ...>

    file/path:  multiple paths for directories and/or PNG files`)
    .help()
    .alias('help', 'h'),

  maybe = f => (value, ...rest) => value ? f(value, ...rest) : false,

  get_stat = async path => ({
    path,
    'stat': await fsp.stat(path).catch(() => false)
  }),

  process_stat = target_promise => {
    return target_promise
      .then(({stat, path}) => {
        if(!stat) {
          return false
        }

        if(stat.isFile()) {
          return path
        }

        if(stat.isDirectory()) {
          return readDir(path)
        }

        return false
      })
  },

  readDir = parent => fsp.readdir(parent)
    .then(dir_items => dir_items.map(name => path.join(parent, name)))
    .then(process_targets)
    .catch(() => false),

  process_targets = list => Promise.all(
    list
      .map(get_stat)
      .map(process_stat)
  ),

  flatten_targets = list => {
    if(!Array.isArray(list)) {
      return [ list ]
    }

    return list.reduce((acc, item) => (
      [ ...acc
      , ...flatten_targets(item)
      ]
    ), [])
  },

  read_png = () => {
    let
      // Keep count of opened filed
      open_count = 0

    return maybe(file => new Promise((resolve, reject) => {
      open_count += 1

      // Avoid `EMFILE, too many open files` error and silently fail after
      // opening 512 files.
      if(512 < open_count) {
        return resolve(false)
      }

      fs.createReadStream(file)
        .pipe(new PNG())
        .on('parsed', function() {
          open_count -= 1

          resolve(this)
        })
        .on('error', () => {
          open_count -= 1

          resolve(false)
        })
    }))
  },

  read_qr = maybe(image => {
    try {
      return qr(image.data, image.width, image.height).data
    }
    catch(ex) {
      return false
    }
  }),

  decode_otp_data = code => {
    try {
      const
        url = new URL(code)

      if('otpauth:' === url.protocol) {
        return [
          url.searchParams.get('issuer') || unescape(url.pathname).substr(1),
          url.searchParams.get('secret'),
          authenticator.generate(url.searchParams.get('secret'))
        ]
      }
    }
    catch(_) {}

    return false
  },

  create_display_table = data => {
    const
      rows = data
        .filter(v => v)
        .map(v => [ v[0].bold, v[1], v[2] ]),

      table = new Table({
        'head': [ 'Service', 'Key', 'Token' ].map(v => v.bold.blue)
      })

    if(1 > rows.length) {
      return ''
    }

    table.push(...rows)

    return table
  }

Promise
  .resolve(argv._)
  .then(args => (1 > args.length) ? [ '.' ] : args)
  // Get file system stat for each target given & flatten the array of promises
  // to get values
  .then(process_targets)
  .then(flatten_targets)
  .then(files => Promise.all(files.map(read_png())))
  .then(images => images
    .map(read_qr)
    .map(decode_otp_data)
  )
  .then(create_display_table)
  .then(output => console.log(output.toString()))
  .catch(error => console.error(error))
